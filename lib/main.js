'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _spin = require('spin.js');

var _spin2 = _interopRequireDefault(_spin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ReactSpinner = function (_PureComponent) {
  _inherits(ReactSpinner, _PureComponent);

  function ReactSpinner() {
    _classCallCheck(this, ReactSpinner);

    return _possibleConstructorReturn(this, (ReactSpinner.__proto__ || Object.getPrototypeOf(ReactSpinner)).apply(this, arguments));
  }

  _createClass(ReactSpinner, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.spinner = new _spin2.default(this.props.config);
      if (!this.props.stopped) {
        this.spinner.spin(this.container);
      }
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(newProps) {
      if (newProps.stopped === true && !this.props.stopped) {
        this.spinner.stop();
      } else if (!newProps.stopped && this.props.stopped === true) {
        this.spinner.spin(this.container);
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.spinner.stop();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement('span', { ref: function ref(container) {
          return _this2.container = container;
        } });
    }
  }]);

  return ReactSpinner;
}(_react.PureComponent);

ReactSpinner.propTypes = {
  config: _propTypes2.default.object,
  stopped: _propTypes2.default.bool
};
exports.default = ReactSpinner;
